(function ($) {
    class Modal {
        constructor(element, options) {
            this.default = {
                closeClass: 'close-modal',
                autoClose: false,
                autoCloseTime: 1000,
                opacity: .7,
                position: 'bottom',
                duration: 500
            };
            this.modal = element;
            this.options = $.extend(this.default, options);
            this.overlay = $('.overlay');
        }

        init() {
            this.showOverlay();
            this.showModal();
            this.events();

            if (this.options.autoClose) {
                this.timeout = setTimeout(this.closeModal(), this.options.autoClose);
            }
        }

        events() {
            this.overlay.on('click', (e) => this.closeModal());
            $(`.${this.options.closeClass}`).on('click', (e) => this.closeModal());
        }

        clearEvents() {
            this.overlay.off('click');
            $(`.${this.options.closeClass}`).off('click');
        }

        showOverlay() {
            if (!this.overlay.length) {
                this.overlay = $('<div class="overlay"></div>');
            }

            this.overlay.css({
                'display': 'block',
                'position': 'fixed',
                'top': '0',
                'left': '0',
                'right': '0',
                'bottom': '0',
                'opacity': '0',
                'background-color': `rgba(0, 0, 0, ${this.options.opacity})`,
                'z-index': '999'
            });

            this.modal.before(this.overlay);
        }

        showModal() {
            let halfWidth = this.modal.outerWidth() / 2;
            let halfHeight = this.modal.outerHeight() / 2;
            let marginTop = `-${halfHeight}px`;
            let marginLeft = `-${halfWidth}px`;
            let top = `50%`;
            const windowHeight = $(document).height();

            if (this.options.position === 'top') {
                top = `100px`;
                marginTop = 0;
            } else if (this.options.position === 'bottom') {
                top = `${windowHeight - this.modal.outerHeight() - 100}px`;
                marginTop = 0;
            }

            this.overlay.animate({
                opacity: 1
            }, this.options.duration);

            this.modal.css({
                'display': 'block',
                'position': 'fixed',
                'top': `${top}`,
                'left': '50%',
                'z-index': '1000',
                'opacity': '0',
                'margin-top': `${marginTop}`,
                'margin-left': `${marginLeft}`
            }).animate({
                opacity: 1
            }, this.options.duration);
        }

        closeModal() {
            this.overlay.animate({
                opacity: 1
            }, this.options.duration, () => this.overlay.css({'display': 'none'}));

            this.modal.animate({
                opacity: 1
            }, this.options.duration, () => this.modal.css({'display': 'none'}));

            clearTimeout(this.timeout);
            this.clearEvents();
        }
    }

    $.fn.easyModal = function (options) {
        new Modal(this, options).init();
    }
}(jQuery));
