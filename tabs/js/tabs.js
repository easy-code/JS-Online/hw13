(function ($) {
    // Tabs
    const controls = $('.nav-tabs li a');

    // Add Event Listener
    controls.on('click', tabs);

    // Click Handler
    function tabs(e) {
        e.preventDefault();

        if ($(this).closest('li').hasClass('active')) return;

        // Find parents
        const list = $(this).closest('ul');
        const tabContent = list.next();
        const target = $($(this).attr('href'));
        const tabActive = tabContent.find('tab-pane.active');

        // Set active class
        list.find('li.active').removeClass('active');
        $(this).closest('li').addClass('active');

        // Show acive tab
        tabActive.fadeOut('fast', () => {
            tabActive.removeClass('active');
            target.fadeIn('fast', () => target.addClass('active'));
        })
    }
}(jQuery));